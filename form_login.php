<?php
	if(isset($_GET['message']))
	{
		if($_GET['message']=='nologin')
		{
			echo "Anda harus login terlebih dahulu<br>";
		}
		elseif($_GET['message']=='logout')
		{
			echo "Anda berhasil logout";
		}
		elseif($_GET['message']=="blabla")
		{
			echo "Anda menulis blabla";
		}
	}
?>
<html>
<head>
	<title>Form Login</title>
	<style>
		body {
			background: #a9c9fc;
			font-family: 'Open Sans', sans-serif;
		}
		.login {
			width: 400px;
			margin: 16px auto;
			font-size: 16px;
		}
		.login-header,
		.login p {
			margin-top: 0;
			margin-bottom: 0;
		}
		.login-header {
			background: #28d;
			padding: 20px;
			font-size: 1.4em;
			font-weight: normal;
			text-align: center;
			text-transform: uppercase;
			color: #fff;
		}
		.login-container {
			background: #ebebeb;
			padding: 12px;
		}
		.login p {
			padding: 12px;
		}
		.login input {
			box-sizing: border-box;
			display: block;
			width: 100%;
			border-width: 1px;
			border-style: solid;
			padding: 16px;
			outline: 0;
			font-family: inherit;
			font-size: 0.95em;
		}
		.login input[type="email"],
		.login input[type="password"] {
			background: #fff;
			border-color: #bbb;
			color: #555;
		}
		
		.login input[type="email"]:focus,
		.login input[type="password"]:focus {
			border-color: #888;
		}
		.login input[type="submit"] {
			background: #28d;
			border-color: transparent;
			color: #fff;
			cursor: pointer;
		}
		.login input[type="submit"]:hover {
			background: #17c;
		}
		
		.login input[type="submit"]:focus {
			border-color: #05a;
		}
	</style>
</head>
<body>
<div class="login">
    <h2 class="login-header">Form Login</h2>
        <form class="login-container" method="post" action="aksilogin.php">
            <p>
                <input type="text" name="uname" required placeholder="username">
            </p>
            <p>
                <input type="password" placeholder="Password" name="passwd" required>
            </p>
            <p>
                <input type="submit" value="Log in">
            </p>
        </form>
</div>
</body>
</html>

