<html>
<?php
	session_start();
	if($_SESSION['isLogin'] != true || $_SESSION['jam_selesai']==date("Y-m-d H:i:s"))
	{
		header("Location: form_login.php?message=nologin");
	}
	echo "Selamat datang, ",strtoupper($_SESSION['uname'])," login pada: ",$_SESSION['jam_mulai'];
	echo "<br>";
?>
    <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <script src="js/bootstrap.min.js"></script>
    </head>
    <body>

        <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Formulir Data Diri</a>
    </div>
      <li><a href="logout.php"> Logout</a></li>
    </ul>
  </div>
</nav>
<br><br>

        <div class="container">   
            <form action="simpandata.php" method="POST">
            <div class="row mb-3">
                <label for="inputNamaLengkap" class="col-sm-2 col-form-label">Nama Lengkap </label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="inputNamaLengkap" name="namaLengkap" placeholder="Masukan Nama Anda ...">
                </div>
            </div>
            <div class="row mb-3">
                <label for="inputUsername" class="col-sm-2 col-form-label">Username </label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="inputNamaLengkap" name="Username" value="<?php echo $_SESSION['uname']?>">
                </div>
            </div>
            <div class="row mb-3">
                <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                <textarea class="form-control" id="inputAlamat" rows="3" placeholder="Masukan Alamat Anda ..." name="alamat"></textarea>
                </div>
            </div>
            <fieldset class="row mb-3">
                <legend class="col-form-label col-sm-2 pt-0">Jenis Kelamin</legend>
                <div class="col-sm-10">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="jenisKelamin" id="gridRadios1" value="Laki-laki">
                    <label class="form-check-label" for="gridRadios1">
                    Laki-laki
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="jenisKelamin" id="gridRadios2" value="Perempuan">
                    <label class="form-check-label" for="gridRadios2">
                    Perempuan
                    </label>
                </div>
                </div>
            </fieldset>
            <fieldset class="row mb-3">
                <legend class="col-form-label col-sm-2 pt-0">Hobi</legend>
                <div class="col-sm-10">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck1" value="Olahraga" name="hobi[]">
                    <label class="form-check-label" for="gridCheck1">
                    Olahraga
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck1" value="Musik" name="hobi[]">
                    <label class="form-check-label" for="gridCheck1">
                    Musik
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck1" value="Belajar" name="hobi[]">
                    <label class="form-check-label" for="gridCheck1">
                    Belajar
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck1" value="Petualang" name="hobi[]">
                    <label class="form-check-label" for="gridCheck1">
                    Petualang
                    </label>
                </div>
                </div>
            </fieldset>

            <div class="row mb-3">
                <label for="inputUsername" class="col-sm-2 col-form-label">Username :</label>
                <div class="col-sm-10">
                    <select class="form-select" aria-label="Default select example" name="Pekerjaan">
                        <option selected>Pekerjaan</option>
                        <option value="PNS">PNS</option>
                        <option value="Wiraswata">Wiraswasta</option>
                        <option value="Wirausaha">Wirausaha</option>
                        <option value="Pegawai BUMN">Pegawai BUMN</option>
                        <option value="Pegawai BUMD">Pegawai BUMD</option>
                        <option value="TNI/Polri">TNI/Polri</option>
                    </select>
                </div>
            </div>
            
            <button type="submit" class="btn btn-primary" name="submit">Submit</button>

            </form>

        </div>
    </body>
</html>

