<?php
if (isset($_POST['submit'])) {
	$nama = $_POST['namaLengkap'];
    $user = $_POST['Username'];
    $alamat = $_POST['alamat'];
    $gender = $_POST['jenisKelamin'];
    $hobies = $_POST['hobi'];
    $work = $_POST['Pekerjaan'];

}
?>
<html>
    <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <script src="js/bootstrap.min.js"></script>
    </head>
    <body>

        <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Formulir Data Diri</a>
    </div>
  </div>
</nav>
<br><br>

        <div class="container">   
        <table class="table table-bordered">
            <thead>
                <tr class="d-flex">
                <th scope="col" class="col-3">Kategori</th>
                <th scope="col" class="col-9">Data</th>
                </tr>
            </thead>
            <tbody>
                <tr class="d-flex">
                <th scope="row" class="col-3">Nama Lengkap</th>
                <td class="col-9"><?php echo $nama ?></td>
                </tr>
                <tr class="d-flex">
                <th scope="row" class="col-3">Username</th>
                <td class="col-9"><?php echo $user ?> </td>
                </tr>
                <tr class="d-flex">
                <th scope="row" class="col-3">Alamat</th>
                <td class="col-9"><?php echo $alamat ?> </td>
                </tr>
                <tr class="d-flex">
                <th scope="row" class="col-3">Jenis Kelamin</th>
                <td class="col-9"><?php echo $gender ?> </td>
                </tr>
                <tr class="d-flex">
                <th scope="row" class="col-3">Hobi</th>
                <td class="col-9"><?php 
                    foreach ($hobies as $hobi) {
                        echo $hobi."<br>";
                    }
                ?> </td>
                </tr>
                <tr class="d-flex">
                <th scope="row" class="col-3">Pekerjaan</th>
                <td class="col-9"><?php echo $work ?> </td>
                </tr>
            </tbody>
            </table>
         </div>
    </body>
</html>

